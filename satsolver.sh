#!/bin/bash

root="$1"
export schemahome="$HOME/Dropbox/Programming/Schema"

sbcl --load "$schemahome/schema.cl" --eval "(instantiate \"$root.wff\" \"$root.scnf\")" --script /dev/null >& satsolver.errors
sbcl --load "$schemahome/schema.cl" --eval "(propositionalize \"$root.scnf\" \"$root.cnf\" \"$root.map\")" --script /dev/null >> satsolver.errors 2>&1

# Sat solver is kissat
kissat "$root.cnf" >& "$root.kissout"
cat "$root.cnf" >> satsolver.errors

if grep -q '^s SATISFIABLE' "$root.kissout"
then
    lits=$(grep 'v ' "$root.kissout" | sed s/v// )  
    echo -n > "$root.soln"   
    for i in $lits
    do
        if (( i > 0 ))
        then
            echo $i ' ' >> "$root.soln" 
        fi
    done

    sbcl --load "$schemahome/schema.cl" --eval "(interpret \"${root}.soln\" \"${root}.map\" \"${root}.out\")" --script /dev/null >> satsolver.errors 2>&1
    echo 'SAT['
    cat "$root.out"
    echo ']'

else
    echo 'UNSAT[]'
fi



