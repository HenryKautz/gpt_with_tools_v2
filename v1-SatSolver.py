from Tool import Tool, msg_list
from gpt_with_tools import gpt_with_tools_dir
import re
import subprocess


class SatSolver(Tool):
    
    name='SatSolver'
    pattern = r'(?m)SOLVE\[(.+)\]'

    def run(self,match:re.Match)->str:
        expression = match.group(1)
        value=None
        try:
            # Call to SCHEMA and a SAT solver here
            with open("gpt.wff", "w") as schemaFile:
                schemaFile.write(expression)
            # Run solver
            # Get result from file solver creates
            sp = subprocess.run(f"{gpt_with_tools_dir}/satsolver.sh", "gpt", capture_output=True, text=True)
            return sp.stdout

        except:
            print(f"SatSolver error!")
            print(f"  Input expression is {expression}")
            print(f"  Returned value is {value}")
            return('SatSolver failed')
    
    instructions = [
        {'role':'user', 'content':
            'In order to help you reason logically, I am giving you the ability '
            'to use a satisfiability solver for propositional logic. '
            'When you want to find a satisfying assigning to logical formal, you '
            'should output SOLVE[<formula>], where <formula> is a formula in prenex '
            'form Boolean logic using LISP notation. '
            'The formula may span multiple lines, and can include comments '
            'using lines that begin with a semi-colon ";". '
            'The next input you read will be the resulting of running a satisfiability '
            'solver on the formula.  If the formula is unsatisfiable, this will be '
            'the UNSAT[].  If the formula is satisfiable, this will be in the '
            'form SAT[<propositions>] where <propositions is a series of ground propositions '
            'separated by whitespace which represent the propositions that are true in '
            'some satisfying assignment. '
        },

        {'role':'assistant', 'content':
            'Okay, I understand.'
        },

        {'role':'user', 'content':
            'Here is an example.  Suppose I write '
            '\n'
            'John and Mary each own a house. '
            'One house is red and the other is blue. '
            'Each has a different pet that is either a cat or a dog. '
            'The cat lives in the blue house. '
            "Mary does not own a dog. "
            'Who owns which color house and which pet?'
        },
        {'role':'assistant', 'content':
            'I will represent the problem in logic and call a satisfiabilitty solver '
            'using the following propositions:\n'
            "JohnRed: John's house is red \n"
            "JohnBlue: John's house is blue \n"
            "MaryRed: Mary's house is red \n"
            "MaryBlue: Mary's house is blue \n" 
            "JohnCat: John owns the cat \n"
            "JohnDog: John owns the dog \n"
            "MaryCat: Mary owns the cat \n"
            "MaryDog: Mary owns the dog \n"            
            
            'SOLVE['
                '(AND \n'
                    "; One house is red and other is blue \n"
                    '(or JohnRed JohnBlue) \n'
                    '(or MaryRed MaryBlue) \n'
                    '(not (and JohnRed JohnBlue)) \n'
                    '(not (and MaryRed MaryBlue)) \n'
                    '(not (and JohnRed MaryRed)) \n'
                    '(not (and JohnBlue MaryBlue)) \n'
                    "; Each of John and Mary has a different pet that is either a cat or a dog \n"
                    '(or JohnCat JohnDog) \n'
                    '(or MaryCat MaryDog) \n'
                    '(not (and JohnCat JohnDog)) \n'
                    '(not (and MaryCat MaryDog)) \n'
                    '(not (and JohnCat MaryCat)) \n'
                    '(not (and JohnDog MaryDog)) \n'  
                    "; The cat lives in the blue house. \n"  
                    '(or (and JohnBlue JohnCat) (and MaryBlue MaryCat) \n'
                    "; Mary does not own a dog. \n"
                    '(not MaryDog) \n'
                ')'
        },
        {'role':'user', 'content':
            'SAT[ JohnRed MaryBlue JohnDog MaryCat ]'
        },
        {'role':'assistant', 'content':
            'John lives in the red house and owns the dog. Mary lives in the blue house and owns the cat.' 
        },
        {'role':'user', 'content':
            'Four ambassadors from Bulgaria, Croatia, Spain, and Sweden stand side by side, each wearing '
            'a uniquely colored tie and representing their respective country at a meeting in the USA. '
            'Your task is to deduce who wears which colored tie, how many years of experience they have, '
            'and their field of expertise, which includes economics, history, politics, and sports. '
            "Determine the specific details of each ambassador's profile.\n"
            'The ambassador who is an expert in History is in the second position.\n'
            'The ambassador who is an expert in Economics is in the first position.\n'
            'The ambassador who is an expert in Sports is at one end.\n'
            'The ambassador from Croatia has 8 years of experience.\n'
            'The ambassador from Bulgaria is an expert in Politics.\n'
            'The ambassador with 7 years of experience is in the last position.\n'
            'The ambassador with 7 years of experience is somewhere to the right of the person wearing the Purple tie.\n'
            'The person wearing the Red tie has 8 years of experience.\n'
            'The ambassador from Croatia is at one of the ends.\n'
            'The ambassador from Spain is in the last position.\n'
            'The person wearing the Yellow tie is somewhere to the left of the ambassador with 2 years of experience.\n'
            '\n'
            'The puzzle is a kind of problem called a “Zebra Puzzle”.  In a Zebra Puzzle, '
            'you have to the determine the properties of a set of entities.  Each entity has '
            'different unique values for the properties.  It is an implicit constraint in Zebra '
            'Puzzle that \n'
            '1. Each entry has a value for each property.  '
            'For example, the ambassador at position 1 has a tie color.\n'
            '2. Each entity has no more than one value for each property. '
            'For example, if the ambassador at position 1 has a yellow tie, he does not '
            'also have a red tie.\n'
            '3. No two entities have the same value for a given property.  For example, '
            'if the ambassador at position 1 has a yellow tie, then the ambassador '
            'at position 2 does not have a yellow tie.\n'
            'We call the set of all of these contraints the uniqueness constraints '
            'for the puzzle. '
            'Remember to be consistent about what are entities and what are properites. '
            'Each proposition in your encoding relates a entity to a property. '
            'For example, a proposition in this puzzle might be that the entity position 1 has '
            'the value yellow for its tie property.'
        },
        {'role':'assistant', 'content':
            'I will call the solver to find a solution to this Zebra puzzle and\n'
            'include all the uniqueness constraints in the logical encoding.\n'
            'SOLVE[\n'
            '(AND\n'
            '  ; Explicit constraints\n'
            '  HistoryPos2\n'
            '  EconomicsPos1\n'
            '  (OR SportsPos1 SportsPos4)\n'
            '  CroatiaYears8\n'
            '  BulgariaPolitics\n'
            '  Years7Pos4\n'
            '  (IMPLIES PurplePos1 (OR Years7Pos2 Years7Pos3 Years7Pos4))\n'
            '  (IMPLIES PurplePos2 (OR Years7Pos3 Years7Pos4))\n'
            '  (IMPLIES PurplePos3 Years7Pos4)\n'
            '  RedYears8\n'
            '  (OR CroatiaPos1 CroatiaPos4)\n'
            '  SpainPos4\n'
            '  (IMPLIES YellowPos1 (OR Years2Pos2 Years2Pos3 Years2Pos4))\n'
            '  (IMPLIES YellowPos2 (OR Years2Pos3 Years2Pos4))\n'
            '  (IMPLIES YellowPos3 Years2Pos4)\n'
            '  \n'
            '  ; Implicit uniqueness constraints\n'
            '  ; Nationalities\n'
            '  (AND\n'
            '    (IMPLIES BulgariaPos1 (AND (NOT CroatiaPos1) (NOT SpainPos1) (NOT SwedenPos1)))\n'
            '    (IMPLIES BulgariaPos2 (AND (NOT CroatiaPos2) (NOT SpainPos2) (NOT SwedenPos2)))\n'
            '    (IMPLIES BulgariaPos3 (AND (NOT CroatiaPos3) (NOT SpainPos3) (NOT SwedenPos3)))\n'
            '    (IMPLIES BulgariaPos4 (AND (NOT CroatiaPos4) (NOT SpainPos4) (NOT SwedenPos4)))\n'
            '    (IMPLIES CroatiaPos1 (AND (NOT BulgariaPos1) (NOT SpainPos1) (NOT SwedenPos1)))\n'
            '    (IMPLIES CroatiaPos2 (AND (NOT BulgariaPos2) (NOT SpainPos2) (NOT SwedenPos2)))\n'
            '    (IMPLIES CroatiaPos3 (AND (NOT BulgariaPos3) (NOT SpainPos3) (NOT SwedenPos3)))\n'
            '    (IMPLIES CroatiaPos4 (AND (NOT BulgariaPos4) (NOT SpainPos4) (NOT SwedenPos4)))\n'
            '    (IMPLIES SpainPos1 (AND (NOT BulgariaPos1) (NOT CroatiaPos1) (NOT SwedenPos1)))\n'
            '    (IMPLIES SpainPos2 (AND (NOT BulgariaPos2) (NOT CroatiaPos2) (NOT SwedenPos2)))\n'
            '    (IMPLIES SpainPos3 (AND (NOT BulgariaPos3) (NOT CroatiaPos3) (NOT SwedenPos3)))\n'
            '    (IMPLIES SpainPos4 (AND (NOT BulgariaPos4) (NOT CroatiaPos4) (NOT SwedenPos4)))\n'
            '    (IMPLIES SwedenPos1 (AND (NOT BulgariaPos1) (NOT CroatiaPos1) (NOT SpainPos1)))\n'
            '    (IMPLIES SwedenPos2 (AND (NOT BulgariaPos2) (NOT CroatiaPos2) (NOT SpainPos2)))\n'
            '    (IMPLIES SwedenPos3 (AND (NOT BulgariaPos3) (NOT CroatiaPos3) (NOT SpainPos3)))\n'
            '    (IMPLIES SwedenPos4 (AND (NOT BulgariaPos4) (NOT CroatiaPos4) (NOT SpainPos4)))\n'
            '  )\n'
            '\n'
            '  ; Tie colors\n'
            '  (AND\n'
            '    (IMPLIES RedPos1 (AND (NOT PurplePos1) (NOT YellowPos1) (NOT BluePos1)))\n'
            '    (IMPLIES RedPos2 (AND (NOT PurplePos2) (NOT YellowPos2) (NOT BluePos2)))\n'
            '    (IMPLIES RedPos3 (AND (NOT PurplePos3) (NOT YellowPos3) (NOT BluePos3)))\n'
            '    (IMPLIES RedPos4 (AND (NOT PurplePos4) (NOT YellowPos4) (NOT BluePos4)))\n'
            '    (IMPLIES PurplePos1 (AND (NOT RedPos1) (NOT YellowPos1) (NOT BluePos1)))\n'
            '    (IMPLIES PurplePos2 (AND (NOT RedPos2) (NOT YellowPos2) (NOT BluePos2)))\n'
            '    (IMPLIES PurplePos3 (AND (NOT RedPos3) (NOT YellowPos3) (NOT BluePos3)))\n'
            '    (IMPLIES PurplePos4 (AND (NOT RedPos4) (NOT YellowPos4) (NOT BluePos4)))\n'
            '    (IMPLIES YellowPos1 (AND (NOT RedPos1) (NOT PurplePos1) (NOT BluePos1)))\n'
            '    (IMPLIES YellowPos2 (AND (NOT RedPos2) (NOT PurplePos2) (NOT BluePos2)))\n'
            '    (IMPLIES YellowPos3 (AND (NOT RedPos3) (NOT PurplePos3) (NOT BluePos3)))\n'
            '    (IMPLIES YellowPos4 (AND (NOT RedPos4) (NOT PurplePos4) (NOT BluePos4)))\n'
            '    (IMPLIES BluePos1 (AND (NOT RedPos1) (NOT PurplePos1) (NOT YellowPos1)))\n'
            '    (IMPLIES BluePos2 (AND (NOT RedPos2) (NOT PurplePos2) (NOT YellowPos2)))\n'
            '    (IMPLIES BluePos3 (AND (NOT RedPos3) (NOT PurplePos3) (NOT YellowPos3)))\n'
            '    (IMPLIES BluePos4 (AND (NOT RedPos4) (NOT PurplePos4) (NOT YellowPos4)))\n'
            '  )\n'
            '  \n'
            '  ; Years of experience\n'
            '  (AND\n'
            '    (IMPLIES Years2Pos1 (AND (NOT Years5Pos1) (NOT Years7Pos1) (NOT Years8Pos1)))\n'
            '    (IMPLIES Years2Pos2 (AND (NOT Years5Pos2) (NOT Years7Pos2) (NOT Years8Pos2)))\n'
            '    (IMPLIES Years2Pos3 (AND (NOT Years5Pos3) (NOT Years7Pos3) (NOT Years8Pos3)))\n'
            '    (IMPLIES Years2Pos4 (AND (NOT Years5Pos4) (NOT Years7Pos4) (NOT Years8Pos4)))\n'
            '    (IMPLIES Years5Pos1 (AND (NOT Years2Pos1) (NOT Years7Pos1) (NOT Years8Pos1)))\n'
            '    (IMPLIES Years5Pos2 (AND (NOT Years2Pos2) (NOT Years7Pos2) (NOT Years8Pos2)))\n'
            '    (IMPLIES Years5Pos3 (AND (NOT Years2Pos3) (NOT Years7Pos3) (NOT Years8Pos3)))\n'
            '    (IMPLIES Years5Pos4 (AND (NOT Years2Pos4) (NOT Years7Pos4) (NOT Years8Pos4)))\n'
            '    (IMPLIES Years7Pos1 (AND (NOT Years2Pos1) (NOT Years5Pos1) (NOT Years8Pos1)))\n'
            '    (IMPLIES Years7Pos2 (AND (NOT Years2Pos2) (NOT Years5Pos2) (NOT Years8Pos2)))\n'
            '    (IMPLIES Years7Pos3 (AND (NOT Years2Pos3) (NOT Years5Pos3) (NOT Years8Pos3)))\n'
            '    (IMPLIES Years7Pos4 (AND (NOT Years2Pos4) (NOT Years5Pos4) (NOT Years8Pos4)))\n'
            '    (IMPLIES Years8Pos1 (AND (NOT Years2Pos1) (NOT Years5Pos1) (NOT Years7Pos1)))\n'
            '    (IMPLIES Years8Pos2 (AND (NOT Years2Pos2) (NOT Years5Pos2) (NOT Years7Pos2)))\n'
            '    (IMPLIES Years8Pos3 (AND (NOT Years2Pos3) (NOT Years5Pos3) (NOT Years7Pos3)))\n'
            '    (IMPLIES Years8Pos4 (AND (NOT Years2Pos4) (NOT Years5Pos4) (NOT Years7Pos4)))\n'
            '  )\n'
            '  \n'
            '  ; Fields of expertise\n'
            '  (AND\n'
            '    (IMPLIES EconomicsPos1 (AND (NOT HistoryPos1) (NOT PoliticsPos1) (NOT SportsPos1)))\n'
            '    (IMPLIES EconomicsPos2 (AND (NOT HistoryPos2) (NOT PoliticsPos2) (NOT SportsPos2)))\n'
            '    (IMPLIES EconomicsPos3 (AND (NOT HistoryPos3) (NOT PoliticsPos3) (NOT SportsPos3)))\n'
            '    (IMPLIES EconomicsPos4 (AND (NOT HistoryPos4) (NOT PoliticsPos4) (NOT SportsPos4)))\n'
            '    (IMPLIES HistoryPos1 (AND (NOT EconomicsPos1) (NOT PoliticsPos1) (NOT SportsPos1)))\n'
            '    (IMPLIES HistoryPos2 (AND (NOT EconomicsPos2) (NOT PoliticsPos2) (NOT SportsPos2)))\n'
            '    (IMPLIES HistoryPos3 (AND (NOT EconomicsPos3) (NOT PoliticsPos3) (NOT SportsPos3)))\n'
            '    (IMPLIES HistoryPos4 (AND (NOT EconomicsPos4) (NOT PoliticsPos4) (NOT SportsPos4)))\n'
            '    (IMPLIES PoliticsPos1 (AND (NOT EconomicsPos1) (NOT HistoryPos1) (NOT SportsPos1)))\n'
            '    (IMPLIES PoliticsPos2 (AND (NOT EconomicsPos2) (NOT HistoryPos2) (NOT SportsPos2)))\n'
            '    (IMPLIES PoliticsPos3 (AND (NOT EconomicsPos3) (NOT HistoryPos3) (NOT SportsPos3)))\n'
            '    (IMPLIES PoliticsPos4 (AND (NOT EconomicsPos4) (NOT HistoryPos4) (NOT SportsPos4)))\n'
            '    (IMPLIES SportsPos1 (AND (NOT EconomicsPos1) (NOT HistoryPos1) (NOT PoliticsPos1)))\n'
            '    (IMPLIES SportsPos2 (AND (NOT EconomicsPos2) (NOT HistoryPos2) (NOT PoliticsPos2)))\n'
            '    (IMPLIES SportsPos3 (AND (NOT EconomicsPos3) (NOT HistoryPos3) (NOT PoliticsPos3)))\n'
            '    (IMPLIES SportsPos4 (AND (NOT EconomicsPos4) (NOT HistoryPos4) (NOT PoliticsPos4)))\n'
            '  )\n'
            '  \n'
            '  ; Each position has one of the values for each property\n'
            '  (AND\n'
            '    ; Position 1 has one nationality\n'
            '    (OR BulgariaPos1 CroatiaPos1 SpainPos1 SwedenPos1)\n'
            '    ; Position 1 has one tie color\n'
            '    (OR RedPos1 PurplePos1 YellowPos1 BluePos1)\n'
            '    ; Position 1 has one years of experience\n'
            '    (OR Years2Pos1 Years5Pos1 Years7Pos1 Years8Pos1)\n'
            '    ; Position 1 has one field of expertise\n'
            '    (OR EconomicsPos1 HistoryPos1 PoliticsPos1 SportsPos1)\n'
            '\n'
            '    ; Position 2 has one nationality\n'
            '    (OR BulgariaPos2 CroatiaPos2 SpainPos2 SwedenPos2)\n'
            '    ; Position 2 has one tie color\n'
            '    (OR RedPos2 PurplePos2 YellowPos2 BluePos2)\n'
            '    ; Position 2 has one years of experience\n'
            '    (OR Years2Pos2 Years5Pos2 Years7Pos2 Years8Pos2)\n'
            '    ; Position 2 has one field of expertise\n'
            '    (OR EconomicsPos2 HistoryPos2 PoliticsPos2 SportsPos2)\n'
            '\n'
            '    ; Position 3 has one nationality\n'
            '    (OR BulgariaPos3 CroatiaPos3 SpainPos3 SwedenPos3)\n'
            '    ; Position 3 has one tie color\n'
            '    (OR RedPos3 PurplePos3 YellowPos3 BluePos3)\n'
            '    ; Position 3 has one years of experience\n'
            '    (OR Years2Pos3 Years5Pos3 Years7Pos3 Years8Pos3)\n'
            '    ; Position 3 has one field of expertise\n'
            '    (OR EconomicsPos3 HistoryPos3 PoliticsPos3 SportsPos3)\n'
            '\n'
            '    ; Position 4 has one nationality\n'
            '    (OR BulgariaPos4 CroatiaPos4 SpainPos4 SwedenPos4)\n'
            '    ; Position 4 has one tie color\n'
            '    (OR RedPos4 PurplePos4 YellowPos4 BluePos4)\n'
            '    ; Position 4 has one years of experience\n'
            '    (OR Years2Pos4 Years5Pos4 Years7Pos4 Years8Pos4)\n'
            '    ; Position 4 has one field of expertise\n'
            '    (OR EconomicsPos4 HistoryPos4 PoliticsPos4 SportsPos4)\n'
            '  )\n'
            ')\n'
            ']\n'
            '\n'   
         }
    ]