from Tool import Tool, msg_list
import re
from semanticscholar import SemanticScholar
import ast

def S(s)->str:
    if not isinstance(s,str):
        s = ''
    return s

def authors_match(authors1:str, authors2:str, debug=False)->bool:
    # All the authors1 must be in authors2, but not vice-versa
    # Turn arguments into lowercase ASCII
    # Remove punctuation
    # Remove singleton letters
    # Split into words
    if debug:
        print(f"authors_match: authors1='{authors1}' authors2='{authors2}'")
    authors1 = authors1.lower().replace(',',' ').replace('.',' ')
    authors2 = authors2.lower().replace(',',' ').replace('.',' ')
    authors1 = re.sub('\b[a-zA-Z]\b',' ',authors1)
    authors2 = re.sub('\b[a-zA-Z]\b',' ',authors2)
    if debug:
        print(f"authors_match: after subbing: authors1='{authors1}' authors2='{authors2}'")
    for a in ' '.split(authors1):
        if debug:
            print(f"is {a} in {authors2}?")
        if a not in authors2:
            if debug:
                print(f"No, returning False")
            return False
    if debug:
        print(f"All yes, returning True")
    return True


class CitationChecker(Tool):
    
    sch = SemanticScholar()
    name='CitationChecker'
    pattern = r'(?m)^[ \t]*CHECKCITATION\((.+)\)[ \t]*$'
    debug=False

    def run(self,match:re.Match)->str:

        expression = S(match.group(1))
        value=None
        try:
            if self.debug:
                print(f"CitationChecker: expression='{expression}'")
            
            # Split into authors, year, title, and journal
            # ast.literal_eval(s) returns a tuple from s - make sure s includes the () or []
            arguments = ast.literal_eval('(' + expression + ')')
            authors = arguments[0]
            year = arguments[1]
            title = arguments[2]
            journal = arguments[3]
            if self.debug:
                print(f"  authors='{authors}'")
                print(f"  year='{year}'")
                print(f"  title='{title}'")
                print(f"  journal='{journal}'")

            # Do search on title and year.  If something is found, see if any match the authors.
            # Need function to do fuzzy match on authors
            # Semantic scholar interface is very fragile, including authors can make it fail
            if self.debug:
                print(f"CitationChecker: sch.search_paper('{title}', limit=3, year='{year}', fields=['authors','year','title',])")
            results=self.sch.search_paper(title, limit=3, year=year, fields=['authors','year','title'])
            if not results:
                if self.debug:
                    print(f"CitationChecker: results NONE")
                return 'BAD CITATION'
                        
            # Do any of the results match authors?
            num_cites = 0
            for paper in results:
                num_cites += 1
                authors_string = ''
                paper_authors = paper['authors']
                for author in paper_authors:
                    authors_string  += author['name'] + ' '
                if self.debug:
                    print(f"CitationChecker: authors_string='{authors_string}'")
                if authors_match(authors, authors_string, debug=self.debug):
                    if self.debug:
                        print(f"CitationChecker: authors match '{authors}' and '{authors_string}'")
                    return 'GOOD'
                if num_cites>=3:
                    break
            if self.debug:
                    print(f"CitationChecker: no results match authors")
            return 'BAD'
        except:
            print(f"CitationChecker error!")
            print(f"  Input expression is {expression}")
            print(f"  Returned value is {value}")
            return('BAD')
        
    
    instructions = [
        {'role':'user', 'content':
            'In order to provide accurate citations to scientific papers, '
            'I am giving you ability to check citations against a large '
            'database of accurate citations.  Whenever you are about to '
            'cite a paper, you should first check that the citation is correct '
            'by outputting a line of the form:\n'
            "CHECKCITATION('Authors Names','Year published', "
            "'title of paper', 'publisher or name of journal, volume, issue, and page')\n"
            "For example, For example, you would check 'Bryant, R. E. (1986). "
            "Graph-based algorithms for boolean function manipulation. "
            "IEEE Transactions on Computers, (7), 677-691.' by outputting on its "
            "own line\n"
            "CHECKCITATION('Bryant, R. E.','1986', "
            "'Graph-based algorithms for boolean function manipulation', "
            "'IEEE Transactions on Computers, (7), 677-691')\n\n"
            'The user will then reply by saying\n'
            'GOOD'
            'or by saying\n'
            'BAD\n'
            'If GOOD is seen, then the citation. \n'
            'if BAD is seen, then find a different citation and again check it.\n'
            'When all the citations have been checked, write a numbered list '
            'of the citations in plain text in standard citation format without using CHECKCITATION. \n'
            'Do you understand?'
        },
        {'role':'assistant', 'content':
            'Yes, I understand.'
        },
        {'role':'user', 'content':
            'Please provide citations for two foundational papers in string theory.'
        },
        {'role':'assistant', 'content':       
            "CHECKCITATION('E. Witten, J. Schwarz, M. Green', "
            "'1987' "
            "'Superstring theory: Volume 1: Introduction', "
            "'Cambridge University Press')"
        },
        {'role':'user', 'content':
            'GOOD'
        },
        {'role':'assistant', 'content':
            'Thank you for verifying the citation is correct, I will remember it.\n\n'          
            "CHECKCITATION('Witten, E.', '1998', 'String theory: Volume 2, superstring "
            "theory and beyond', 'Cambridge University Press')"
        },
        {'role':'user', 'content':
            'BAD'
        },
        {'role':'user', 'content':
            'I apolgize for suggesting a bad citation.  I will try a different citation.\n\n'
            "CHECKCITATION('Witten, E.', '1995', 'String theory dynamics in various dimensions', "
            "'Nuclear Physics B, 443(1), 85-126')"
        },
        {'role':'user', 'content':
            'GOOD'
        },
        {'role':'assistant', 'content':
            'Thank you for verifying the citation is correct, I will remember it.\n\n'
            'Two foundational papers in string theory are:\n\n'
            '1. E. Witten, J. Schwarz, M. Green, (1987) Superstring Theory: Volume 1: Introduction. Cambridge University Press.\n\n'
            '2. Witten, E. (1995). String theory dynamics in various dimensions. Nuclear Physics B, 443(1), 85-126.'
        }
    ]